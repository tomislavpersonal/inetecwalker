﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WalkerDemo
{
    class Walker
    {
        public int MainAxisPincerARow { get; set; }
        public int MainAxisPincerAColumn { get; set; }
        public int MainAxisPincerBRow { get; set; }
        public int MainAxisPincerBColumn { get; set; }

        public int SecondaryAxisPincerARow { get; set; }
        public int SecondaryAxisPincerAColumn { get; set; }
        public int SecondaryAxisPincerBRow { get; set; }
        public int SecondaryAxisPincerBColumn { get; set; }

        public int ToolHeadRow { get; set; }
        public int ToolHeadColumn { get; set; }
    }
}
