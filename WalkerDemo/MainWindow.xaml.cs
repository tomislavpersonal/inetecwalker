﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using WalkerDemo.ViewModels;
using WalkerDemo.Models;

namespace WalkerDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MainViewModel _MVM;

        public MainWindow()
        {
            InitializeComponent();
            _MVM = this.DataContext as MainViewModel;
        }

        // INFO: Using left and right mouseDown events when selecting start/target tubes to avoid sending View data (mouse args) to ViewModel

        private void tubesheetCanvas_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            Point pt = e.GetPosition((Canvas)sender);
            HitTestResult result = VisualTreeHelper.HitTest(tubesheetCanvas, pt);
            if (result != null)
            {
                if (result.VisualHit is Shape)
                {
                    Shape hitTube = (Shape)result.VisualHit;
                    if (hitTube.Tag != null)
                    {
                        int id = (int)hitTube.Tag;
                        if (!_MVM.trySetTargetTube((int)hitTube.Tag))
                        {
                            MessageBox.Show("Selected tube plugged, please select another");
                        }
                    }
                }
            }
        }

        private void tubesheetCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Point pt = e.GetPosition((Canvas)sender);
            HitTestResult result = VisualTreeHelper.HitTest(tubesheetCanvas, pt);
            if (result != null)
            {
                if (result.VisualHit is Shape)
                {
                    Shape hitTube = (Shape)result.VisualHit;
                    if (hitTube.Tag != null)
                    {
                        int id = (int)hitTube.Tag;
                        if (!_MVM.trySetStartTube((int)hitTube.Tag))
                        {
                            MessageBox.Show("Selected tube plugged, please select another");
                        }
                    }
                }
            }

        }
    }
}
