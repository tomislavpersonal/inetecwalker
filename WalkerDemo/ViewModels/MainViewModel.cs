﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Windows;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Windows.Input;
using WalkerDemo.Models;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Diagnostics;

namespace WalkerDemo.ViewModels
{
    class MainViewModel : ObservableObject
    {
        private TubeSheet _tubesheet = new TubeSheet();
        private Walker _walker;

        public ObservableCollection<UIElement> TubeSheetElements { get; private set; }

        private string _startLabelText = "Left click on tube to set Start";
        private string _targetLabelText = "Right click on tube to set Target";
        private string _infoText = "No Messages";
        private int _walkerOrientation = 0;
        private bool _keepWalking = false;

        //private enum WalkerOrientaionDirection { UpLeft, UpRight, DownLeft, DownRight };

        public MainViewModel()
        {
            TubeSheetElements = new ObservableCollection<UIElement>();
        }


        #region BINDED PROP HANDLERS

        public int WalkerOrientation
        {
            get { return _walkerOrientation; }
            set
            {
                _walkerOrientation = value;
                RaisePropertyChangedEvent("WalkerOrientation");
            }
        }

        public bool KeepWalking
        {
            get { return _keepWalking; }
            set
            {
                _keepWalking = value;
                RaisePropertyChangedEvent("KeepWalking");
            }
        }

        public string InfoText
        {
            get { return _infoText; }
            set
            {
                _infoText = value;
                RaisePropertyChangedEvent("InfoText");
            }
        }

        public string StartLabelText
        {
            get { return _startLabelText; }
            set
            {
                _startLabelText = value;
                RaisePropertyChangedEvent("StartLabelText");
            }
        }

        public string TargetLabelText
        {
            get { return _targetLabelText; }
            set
            {
                _targetLabelText = value;
                RaisePropertyChangedEvent("TargetLabelText");
            }
        }

        #endregion


        #region COMMANDS

        public ICommand GoWalker
        {
            get { return new DelegateCommand(goWalker); }
        }

        public ICommand InitTubeSheet
        {
            get { return new DelegateCommand(initTubesInTubesheet); }
        }

        public ICommand InitWalker
        {
            get { return new DelegateCommand(initWalker); }
        }

        #endregion


        #region AI SECTION

        private void goWalker()
        {
            
            if (_walker == null || _tubesheet.StartTube == null || _tubesheet.TargetTube == null)
            {
                return;
            }

            if (!isTargetTouchable())
            {
                stepWalker();
            }
            else
            { // target tube is touchable without walking, move main axis to set tool head on target tube
                positionToolHead();
            }

               
            _walker.CalculateWalkerMainAxis();
            resetTubeSheet();
            if (reDrawWalker())
            {
                InfoText = "Walker Took a Step";
            }
            else
            {
                InfoText = "Walker step error";
            }

            if (_keepWalking && (_tubesheet.TargetTube.Row != _walker.GetMainAxisHeadTubeRow() || _tubesheet.TargetTube.Column != _walker.GetMainAxisHeadTubeColumn()))
            {
                var timer = new System.Windows.Threading.DispatcherTimer { Interval = TimeSpan.FromMilliseconds(500) };
                timer.Start();
                timer.Tick += (sender, args) =>
                {
                    timer.Stop();
                    goWalker();
                };
            }            
        }

        private void stepWalker()
        {
            // Logic to walk towards target tube until it is in range

            if (_tubesheet.TargetTube.Row - _walker.secondaryAxisPincerATubeRow > 12)
            { // target too high, must move up
                stepWalkerUp();
            }
            else if (_walker.secondaryAxisPincerATubeRow + 2 > _tubesheet.TargetTube.Row)
            { // target too low, must move down
                stepWalkerDown();
            }else{ // target to the side
                sideStepWalker();
            }
        }

        private void stepWalkerDown()
        {
            if (_walker.GetRotation() == 45)
            {
                if (_tubesheet.TargetTube.Column <= _walker.secondaryAxisPincerATubeColumn + 12)
                {
                    // target left of center

                    if (_walker.secondaryAxisPincerATubeRow - _walker.mainAxisPincerBTubeRow > 2)
                    { // move sec axis down
                        int step =_walker.SecondaryAxisPincerATubeRow - _walker.mainAxisPincerBTubeRow - 2;
                        _walker.secondaryAxisPincerATubeColumn = _walker.secondaryAxisPincerATubeColumn - step;
                        _walker.secondaryAxisPincerATubeRow = _walker.secondaryAxisPincerATubeRow - step;

                        _walker.secondaryAxisPincerBTubeColumn = _walker.secondaryAxisPincerBTubeColumn - step;
                        _walker.secondaryAxisPincerBTubeRow = _walker.secondaryAxisPincerBTubeRow - step;
                    }
                    else
                    {  // move main axis down
                        _walker.SetMainAxisHeadTubeColumn(_walker.GetMainAxisHeadTubeColumn() - 10);
                        _walker.SetMainAxisHeadTubeRow(_walker.GetMainAxisHeadTubeRow() - 10);

                        _walker.mainAxisPincerBTubeRow = _walker.mainAxisPincerBTubeRow - 10;
                        _walker.mainAxisPincerBTubeColumn = _walker.mainAxisPincerBTubeColumn - 10;
                    }
                }
                else  // target right of center
                {
                    _walker.Rotate(-45);
                    _walker.SetWalkerOrientation(0);
                    _walker.CalculateWalkerHeadAfterRotation();
                }
            }
            else  // rotate angle is -45
            {
                if (_tubesheet.TargetTube.Column <= _walker.secondaryAxisPincerATubeColumn + 12)
                { // target left of center
                    _walker.Rotate(45);
                    _walker.SetWalkerOrientation(1);
                    _walker.CalculateWalkerHeadAfterRotation();
                }
                else  // target right of center
                {
                    if (_walker.secondaryAxisPincerATubeRow - _walker.mainAxisPincerBTubeRow > 2)
                    { // move sec axis down
                        int step = _walker.SecondaryAxisPincerATubeRow - _walker.mainAxisPincerBTubeRow - 2;
                        _walker.secondaryAxisPincerATubeColumn = _walker.secondaryAxisPincerATubeColumn + step;
                        _walker.secondaryAxisPincerATubeRow = _walker.secondaryAxisPincerATubeRow - step;

                        _walker.secondaryAxisPincerBTubeColumn = _walker.secondaryAxisPincerBTubeColumn + step;
                        _walker.secondaryAxisPincerBTubeRow = _walker.secondaryAxisPincerBTubeRow - step;
                    }
                    else
                    {  // move main axis down
                        _walker.SetMainAxisHeadTubeColumn(_walker.GetMainAxisHeadTubeColumn() + 10);
                        _walker.SetMainAxisHeadTubeRow(_walker.GetMainAxisHeadTubeRow() - 10);

                        _walker.mainAxisPincerBTubeRow = _walker.mainAxisPincerBTubeRow - 10;
                        _walker.mainAxisPincerBTubeColumn = _walker.mainAxisPincerBTubeColumn + 10;
                    }
                }
            }
        }

        private void sideStepWalker()
        {
            if (_walker.GetRotation() == -45)
            {
                if (_tubesheet.TargetTube.Column < _walker.secondaryAxisPincerATubeColumn)
                { // move left
                    int buffer = _walker.GetMainAxisHeadTubeColumn() + _walker.GetMainAxisHeadTubeRow() - _walker.secondaryAxisPincerATubeRow + 1;
                    if (_walker.secondaryAxisPincerBTubeColumn > buffer)
                    {
                        // move sec axis left
                        int step = _walker.secondaryAxisPincerBTubeColumn - buffer;

                        if (_walker.secondaryAxisPincerATubeColumn - step < 1)
                        {
                            step = _walker.secondaryAxisPincerATubeColumn - 1;
                        }
                        _walker.secondaryAxisPincerATubeColumn = _walker.secondaryAxisPincerATubeColumn - step;
                        _walker.secondaryAxisPincerBTubeColumn = _walker.secondaryAxisPincerBTubeColumn - step;

                    }
                    else
                    { // move main axis left
                        _walker.SetMainAxisHeadTubeColumn(_walker.secondaryAxisPincerATubeColumn - _walker.GetMainAxisHeadTubeRow() + _walker.secondaryAxisPincerATubeRow + 1);
                    }
                }
                else if (_tubesheet.TargetTube.Column > _walker.secondaryAxisPincerBTubeColumn)
                { // move right
                    int buffer = _walker.GetMainAxisHeadTubeColumn() + _walker.GetMainAxisHeadTubeRow() - _walker.secondaryAxisPincerATubeRow - _walker.secondaryAxisPincerATubeColumn - 1;
                    if (_tubesheet.tubesheetSideTubeCount - _walker.secondaryAxisPincerBTubeColumn < buffer)
                    {
                        // move sec axis right
                        //int step = buffer - _walker.secondaryAxisPincerATubeColumn;
                        int step = _tubesheet.tubesheetSideTubeCount - _walker.secondaryAxisPincerBTubeColumn;
                        if (_walker.secondaryAxisPincerBTubeColumn + step > _tubesheet.tubesheetSideTubeCount)
                        {
                            step = _tubesheet.tubesheetSideTubeCount - _walker.secondaryAxisPincerBTubeColumn - 1;
                        }
                        _walker.secondaryAxisPincerATubeColumn = _walker.secondaryAxisPincerATubeColumn + step;
                        _walker.secondaryAxisPincerBTubeColumn = _walker.secondaryAxisPincerBTubeColumn + step;

                    }
                    else
                    { // move main axis right
                        _walker.SetMainAxisHeadTubeColumn(_walker.secondaryAxisPincerBTubeColumn + _walker.GetMainAxisHeadTubeRow() - _walker.secondaryAxisPincerATubeRow - 1);
                        _walker.Rotate(45);
                        _walker.SetWalkerOrientation(1);
                        //_walker.CalculateWalkerHeadAfterRotation();
                    }
                }
            }
            else  // rotate angle is 45
            {
                if (_tubesheet.TargetTube.Column < _walker.secondaryAxisPincerATubeColumn)
                { // move left
                    //int buffer = _walker.GetMainAxisHeadTubeColumn() + _walker.GetMainAxisHeadTubeRow() - _walker.secondaryAxisPincerATubeRow + 1;
                    int buffer = _walker.secondaryAxisPincerATubeColumn - (_walker.GetMainAxisHeadTubeColumn() - _walker.GetMainAxisHeadTubeRow() + _walker.secondaryAxisPincerATubeRow);
                    if (_walker.secondaryAxisPincerBTubeColumn > buffer)
                    {
                        // move sec axis left
                        int step = _walker.secondaryAxisPincerBTubeColumn - buffer;

                        if (_walker.secondaryAxisPincerATubeColumn - step < 1)
                        {
                            step = _walker.secondaryAxisPincerATubeColumn - 1;
                        }
                        _walker.secondaryAxisPincerATubeColumn = _walker.secondaryAxisPincerATubeColumn - step;
                        _walker.secondaryAxisPincerBTubeColumn = _walker.secondaryAxisPincerBTubeColumn - step;

                    }
                    else
                    { // move main axis left
                        _walker.SetMainAxisHeadTubeColumn(_walker.secondaryAxisPincerATubeColumn - _walker.GetMainAxisHeadTubeRow() + _walker.secondaryAxisPincerATubeRow + 1);
                    }
                }
                else if (_tubesheet.TargetTube.Column > _walker.secondaryAxisPincerBTubeColumn)
                { // move right
                    int buffer = _walker.GetMainAxisHeadTubeColumn() - _walker.GetMainAxisHeadTubeRow() + _walker.secondaryAxisPincerATubeRow - _walker.secondaryAxisPincerATubeColumn - 1;
                    if (_tubesheet.tubesheetSideTubeCount > _walker.secondaryAxisPincerBTubeColumn + buffer)
                    {
                        // move sec axis right
                        //int step = buffer - _walker.secondaryAxisPincerATubeColumn;
                        int step = buffer; // _tubesheet.tubesheetSideTubeCount - _walker.secondaryAxisPincerBTubeColumn;
                        if (_walker.secondaryAxisPincerBTubeColumn + step > _tubesheet.tubesheetSideTubeCount)
                        {
                            step = _tubesheet.tubesheetSideTubeCount - _walker.secondaryAxisPincerBTubeColumn - 1;
                        }
                        _walker.secondaryAxisPincerATubeColumn = _walker.secondaryAxisPincerATubeColumn + step;
                        _walker.secondaryAxisPincerBTubeColumn = _walker.secondaryAxisPincerBTubeColumn + step;
                    }
                    else
                    { // move main axis right
                        _walker.SetMainAxisHeadTubeColumn(_walker.secondaryAxisPincerBTubeColumn + _walker.GetMainAxisHeadTubeRow() - _walker.secondaryAxisPincerATubeRow - 1);
                        _walker.Rotate(-45);
                        _walker.SetWalkerOrientation(0);
                        //_walker.CalculateWalkerHeadAfterRotation();
                    }
                }


            }
        }

        private void stepWalkerUp()
        {
            if (_walker.GetRotation() == -45)
            {
                if (_tubesheet.TargetTube.Column <= _walker.secondaryAxisPincerATubeColumn + 12)
                {
                    // target left of center

                    // move secondary axis up
                    if (_walker.GetMainAxisHeadTubeRow() - _walker.secondaryAxisPincerATubeRow > 2)
                    { // move sec axis up
                        int step = _walker.GetMainAxisHeadTubeRow() - 2 - _walker.SecondaryAxisPincerATubeRow;
                        _walker.secondaryAxisPincerATubeColumn = _walker.secondaryAxisPincerATubeColumn - step;
                        _walker.secondaryAxisPincerATubeRow = _walker.secondaryAxisPincerATubeRow + step;

                        _walker.secondaryAxisPincerBTubeColumn = _walker.secondaryAxisPincerBTubeColumn - step;
                        _walker.secondaryAxisPincerBTubeRow = _walker.secondaryAxisPincerBTubeRow + step;
                    }
                    else
                    {  // move main axis up
                        //int step = 10; // _walker.GetMainAxisHeadTubeColumn() - 2 - _walker.SecondaryAxisPincerATubeColumn;
                        _walker.SetMainAxisHeadTubeColumn(_walker.GetMainAxisHeadTubeColumn() - 10);
                        _walker.SetMainAxisHeadTubeRow(_walker.GetMainAxisHeadTubeRow() + 10);

                        _walker.mainAxisPincerBTubeRow = _walker.mainAxisPincerBTubeRow + 10;
                        _walker.mainAxisPincerBTubeColumn = _walker.mainAxisPincerBTubeColumn - 10;
                    }
                    //if (_tubesheet.TargetTube.Row - _walker.secondaryAxisPincerATubeRow > 12)
                    //{
                    //    //    //int heightDiff = _tubesheet.TargetTube.Row
                    //    //    _walker.SetMainAxisHeadTubeColumn(_walker.GetMainAxisHeadTubeColumn() + _walker.GetMainAxisHeadTubeRow() - _tubesheet.TargetTube.Row);
                    //    //    _walker.SetMainAxisHeadTubeRow(_tubesheet.TargetTube.Row);
                    //}
                    //else
                    //{
                    //    _walker.SetMainAxisHeadTubeColumn(_tubesheet.TargetTube.Column);
                    //}

                }
                else  // target right of center
                {
                    _walker.Rotate(45);
                    _walker.SetWalkerOrientation(1);
                    _walker.CalculateWalkerHeadAfterRotation();
                }
            }
            else  // rotate angle is 45
            {
                if (_tubesheet.TargetTube.Column <= _walker.secondaryAxisPincerATubeColumn + 12)
                { // target left of center
                    _walker.Rotate(-45);
                    _walker.SetWalkerOrientation(0);
                    _walker.CalculateWalkerHeadAfterRotation();
                }
                else  // target right of center
                {
                    if (_walker.GetMainAxisHeadTubeRow() - _walker.secondaryAxisPincerATubeRow > 2)
                    { // move sec axis up
                        int step = _walker.GetMainAxisHeadTubeRow() - 2 - _walker.SecondaryAxisPincerATubeRow;
                        _walker.secondaryAxisPincerATubeColumn = _walker.secondaryAxisPincerATubeColumn + step;
                        _walker.secondaryAxisPincerATubeRow = _walker.secondaryAxisPincerATubeRow + step;

                        _walker.secondaryAxisPincerBTubeColumn = _walker.secondaryAxisPincerBTubeColumn + step;
                        _walker.secondaryAxisPincerBTubeRow = _walker.secondaryAxisPincerBTubeRow + step;
                    }
                    else
                    {  // move main axis up
                        //int step = 10; // _walker.GetMainAxisHeadTubeColumn() - 2 - _walker.SecondaryAxisPincerATubeColumn;
                        _walker.SetMainAxisHeadTubeColumn(_walker.GetMainAxisHeadTubeColumn() + 10);
                        _walker.SetMainAxisHeadTubeRow(_walker.GetMainAxisHeadTubeRow() + 10);

                        _walker.mainAxisPincerBTubeRow = _walker.mainAxisPincerBTubeRow + 10;
                        _walker.mainAxisPincerBTubeColumn = _walker.mainAxisPincerBTubeColumn + 10;
                    }
                    //if (_walker.GetMainAxisHeadTubeRow() != _tubesheet.TargetTube.Row)
                    //{
                    //    _walker.SetMainAxisHeadTubeColumn(_walker.GetMainAxisHeadTubeColumn() - _walker.GetMainAxisHeadTubeRow() + _tubesheet.TargetTube.Row);
                    //    _walker.SetMainAxisHeadTubeRow(_tubesheet.TargetTube.Row);
                    //}
                    //else
                    //{
                    //    _walker.SetMainAxisHeadTubeColumn(_tubesheet.TargetTube.Column);
                    //}
                }
            }
        }

        private void positionToolHead()
        {
            if (_walker.GetRotation() == -45)
            {
                if (_tubesheet.TargetTube.Column <= _walker.secondaryAxisPincerATubeColumn + 12)
                { // target left of center
                    if (_walker.GetMainAxisHeadTubeRow() != _tubesheet.TargetTube.Row)
                    {
                        //int heightDiff = _tubesheet.TargetTube.Row
                        _walker.SetMainAxisHeadTubeColumn(_walker.GetMainAxisHeadTubeColumn() + _walker.GetMainAxisHeadTubeRow() - _tubesheet.TargetTube.Row);
                        _walker.SetMainAxisHeadTubeRow(_tubesheet.TargetTube.Row);
                    }
                    else
                    {
                        _walker.SetMainAxisHeadTubeColumn(_tubesheet.TargetTube.Column);
                    }

                }
                else  // target right of center
                {
                    _walker.Rotate(45);
                    _walker.SetWalkerOrientation(1);
                    _walker.CalculateWalkerHeadAfterRotation();
                }
            }
            else  // rotate angle is 45
            {
                if (_tubesheet.TargetTube.Column <= _walker.secondaryAxisPincerATubeColumn + 12)
                { // target left of center
                    _walker.Rotate(-45);
                    _walker.SetWalkerOrientation(0);
                    _walker.CalculateWalkerHeadAfterRotation();
                }
                else  // target right of center
                {
                    if (_walker.GetMainAxisHeadTubeRow() != _tubesheet.TargetTube.Row)
                    {
                        _walker.SetMainAxisHeadTubeColumn(_walker.GetMainAxisHeadTubeColumn() - _walker.GetMainAxisHeadTubeRow() + _tubesheet.TargetTube.Row);
                        _walker.SetMainAxisHeadTubeRow(_tubesheet.TargetTube.Row);
                    }
                    else
                    {
                        _walker.SetMainAxisHeadTubeColumn(_tubesheet.TargetTube.Column);
                    }
                }
            }
        }

        public bool isTargetTouchable()
        {
            if (_walker.SecondaryAxisPincerATubeRow == _walker.SecondaryAxisPincerBTubeRow)
            {
                // secondary axis horizontal
                if (_walker.GetMainAxisHeadTubeRow() > _walker.SecondaryAxisPincerATubeRow)
                {
                    // head above secondary axis
                    if (_tubesheet.TargetTube.Row < _walker.SecondaryAxisPincerATubeRow + 2 || _tubesheet.TargetTube.Row > _walker.SecondaryAxisPincerATubeRow + 12)
                    {
                        return false;
                    }
                    else
                    {
                        if ((_walker.secondaryAxisPincerATubeColumn - _tubesheet.TargetTube.Column < _tubesheet.TargetTube.Row - _walker.secondaryAxisPincerATubeRow) && (_tubesheet.TargetTube.Column - _walker.secondaryAxisPincerBTubeColumn < _tubesheet.TargetTube.Row - _walker.secondaryAxisPincerATubeRow))
                        {
                            return true;
                        }else{
                            return false;
                        }
                    }

                }
                else
                {
                    // TODO: implemet handling og lower horizontal orientation

                }
            }
            else
            { // TODO: implement handling vertical orientation
                // secondary axis vertical
                if (_walker.GetMainAxisHeadTubeColumn() < _walker.SecondaryAxisPincerATubeColumn)
                {
                    // head left of secondary axis

                }
                else
                {
                    // head right of secondary axis

                }
            }
            return false;
        }

        #endregion

        
        #region MANIPULATION LOGIC

        private void initTubesInTubesheet(){

            _tubesheet.InitiateTubesFromXML();
            resetTubeSheet();
            InfoText = "Tubes Initiated from XML";
        }

        public void initWalker()
        {
            if (_tubesheet.StartTube == null)
            {
                InfoText = "Please set Start Tube";
            }
            else
            {
                resetTubeSheet();

                resetWalker(_walkerOrientation);

                if (reDrawWalker())
                {
                    InfoText = "Walker ready";
                }
                else
                {
                    InfoText = "Walker tubes out of range";
                }

            }
        }

        public void resetWalker(int orientation)
        {
            _walker = new Walker(orientation);

            Tube startTube = _tubesheet.StartTube;
            _walker.SetMainAxisHeadTubeRow(startTube.Row);
            _walker.SetMainAxisHeadTubeColumn(startTube.Column);

            switch (orientation)
            {
                case 0:
                    _walker.Rotate(-45);
                    _walker.TranslateMainAxis(0);
                    _walker.TranslateSecondaryAxis(0);

                    _walker.CalculateWalkerAxes();

                    break;

                case 1:
                    _walker.Rotate(45);
                    _walker.TranslateMainAxis(0);
                    _walker.TranslateSecondaryAxis(0);

                    _walker.CalculateWalkerAxes();

                    break;

                case 2:
                    _walker.Rotate(45);
                    _walker.TranslateMainAxis(0);
                    _walker.TranslateSecondaryAxis(0);

                    _walker.CalculateWalkerAxes();

                    break;

                case 3:
                    _walker.Rotate(-45);
                    _walker.TranslateMainAxis(0);
                    _walker.TranslateSecondaryAxis(0);

                    _walker.CalculateWalkerAxes();

                    break;

            }
        }

        private bool reDrawWalker()
        {
            //if (_walker != null && isWalkerInTubeSheetArea())
            if (_walker != null)
            {
                LockMainAxis();
                LockSecondaryAxis();

                return true;
            }
            else
            {
                return false;
            }
        }

        public bool trySetStartTube(int tubeId)
        {
            Tube currTube = _tubesheet.GetTubeByID(tubeId);

            if (currTube.Status != "Plugged")
            {
                _tubesheet.StartTube = currTube;

                _tubesheet.ResetTubeStartAnchors();

                currTube.Anchor = "Start";

                resetTubesInTubeSheet();

                StartLabelText = "Start Tube, Row: " + _tubesheet.StartTube.Row + ", Column: " + _tubesheet.StartTube.Column;

                return true;
            }
            else
            {
                return false;
            }
        }

        public bool trySetTargetTube(int tubeId)
        {
            Tube currTube = _tubesheet.GetTubeByID(tubeId);

            if (currTube.Status != "Plugged")
            {
                _tubesheet.TargetTube = currTube;

                _tubesheet.ResetTubeTargetAnchors();

                currTube.Anchor = "Target";

                resetTubesInTubeSheet();

                TargetLabelText = "Target Tube, Row: " + _tubesheet.TargetTube.Row + ", Column: " + _tubesheet.TargetTube.Column;

                return true;
            }
            else
            {
                return false;
            }
        }

        public void LockMainAxis()
        {

                Line mainAxis = new Line();
                mainAxis.Name = "MainAxis";
                mainAxis.Visibility = System.Windows.Visibility.Visible;
                mainAxis.StrokeThickness = 20;
                mainAxis.Stroke = System.Windows.Media.Brushes.Green;
                mainAxis.X1 = _tubesheet.GetTubeX(_walker.GetMainAxisHeadTubeColumn());
                mainAxis.X2 = _tubesheet.GetTubeX(_walker.MainAxisPincerBTubeColumn);
                mainAxis.Y1 = _tubesheet.GetTubeY(_walker.GetMainAxisHeadTubeRow(), _tubesheet.tubesheetHeight);
                mainAxis.Y2 = _tubesheet.GetTubeY(_walker.MainAxisPincerBTubeRow, _tubesheet.tubesheetHeight);
                TubeSheetElements.Add(mainAxis);

                // Add tool head
                Shape toolHead = new Ellipse() { Height = 20, Width = 20 };
                RadialGradientBrush brush = new RadialGradientBrush();
                brush.GradientStops.Add(new GradientStop(Colors.Maroon, 1));
                toolHead.Fill = brush;

                Canvas.SetLeft(toolHead, _walker.GetMainAxisHeadTubeColumn() * _tubesheet.tubePitch / 2 - 5);
                Canvas.SetTop(toolHead, _tubesheet.tubesheetHeight - (_walker.GetMainAxisHeadTubeRow() * _tubesheet.tubePitch / 2) - 5);

                toolHead.Name = "MainAxisHead";

                TubeSheetElements.Add(toolHead);

                // Add PincerB
                Shape pincerB = new Ellipse() { Height = 20, Width = 20 };
                brush = new RadialGradientBrush();
                brush.GradientStops.Add(new GradientStop(Colors.Orchid, 1));
                pincerB.Fill = brush;

                Canvas.SetLeft(pincerB, _walker.MainAxisPincerBTubeColumn * _tubesheet.tubePitch / 2 - 5);
                Canvas.SetTop(pincerB, _tubesheet.tubesheetHeight - (_walker.MainAxisPincerBTubeRow * _tubesheet.tubePitch / 2) - 5);

                pincerB.Name = "MainAxisPincerB";

                TubeSheetElements.Add(pincerB);

                // Add PincerA
                Shape pincerA = new Ellipse() { Height = 20, Width = 20 };
                brush = new RadialGradientBrush();
                brush.GradientStops.Add(new GradientStop(Colors.Orchid, 1));
                pincerA.Fill = brush;

                Canvas.SetLeft(pincerA, _walker.MainAxisPincerATubeColumn * _tubesheet.tubePitch / 2 - 5);
                Canvas.SetTop(pincerA, _tubesheet.tubesheetHeight - (_walker.MainAxisPincerATubeRow * _tubesheet.tubePitch / 2) - 5);

                pincerA.Name = "MainAxisPincerA";

                TubeSheetElements.Add(pincerA);

        }

        public void LockSecondaryAxis()
        {
                Line secondaryAxis = new Line();
                secondaryAxis.Name = "SecondaryAxis";
                secondaryAxis.Visibility = System.Windows.Visibility.Visible;
                secondaryAxis.StrokeThickness = 20;
                secondaryAxis.Stroke = System.Windows.Media.Brushes.Peru;
                secondaryAxis.X1 = _tubesheet.GetTubeX(_walker.SecondaryAxisPincerATubeColumn);
                secondaryAxis.X2 = _tubesheet.GetTubeX(_walker.SecondaryAxisPincerBTubeColumn);
                secondaryAxis.Y1 = _tubesheet.GetTubeY(_walker.SecondaryAxisPincerATubeRow, _tubesheet.tubesheetHeight);
                secondaryAxis.Y2 = _tubesheet.GetTubeY(_walker.SecondaryAxisPincerBTubeRow, _tubesheet.tubesheetHeight);
                TubeSheetElements.Add(secondaryAxis);

                RadialGradientBrush brush = new RadialGradientBrush();
                brush.GradientStops.Add(new GradientStop(Colors.Orchid, 1));

                // Add PincerB
                Shape pincerB = new Ellipse() { Height = 20, Width = 20 };
                pincerB.Fill = brush;

                Canvas.SetLeft(pincerB, _walker.SecondaryAxisPincerBTubeColumn * _tubesheet.tubePitch / 2 - 5);
                Canvas.SetTop(pincerB, _tubesheet.tubesheetHeight - (_walker.SecondaryAxisPincerBTubeRow * _tubesheet.tubePitch / 2) - 5);

                pincerB.Name = "SecondaryAxisPincerB";

                TubeSheetElements.Add(pincerB);

                // Add PincerA
                Shape pincerA = new Ellipse() { Height = 20, Width = 20 };
                pincerA.Fill = brush;

                Canvas.SetLeft(pincerA, _walker.SecondaryAxisPincerATubeColumn * _tubesheet.tubePitch / 2 - 5);
                Canvas.SetTop(pincerA, _tubesheet.tubesheetHeight - (_walker.SecondaryAxisPincerATubeRow * _tubesheet.tubePitch / 2) - 5);

                pincerA.Name = "SecondaryAxisPincerA";

                TubeSheetElements.Add(pincerA);
        }

        public void resetTubesInTubeSheet()
        {

            while (TubeSheetElements.Count > 0)
            {
                TubeSheetElements.RemoveAt(0);
            }

            foreach (Tube tube in _tubesheet.tubes)
            {

                Shape newShape = new Ellipse() { Height = _tubesheet.tubeDiameter / 2, Width = _tubesheet.tubeDiameter / 2 };

                RadialGradientBrush brush = new RadialGradientBrush();

                if (tube.Anchor == "Start")
                {
                    brush.GradientStops.Add(new GradientStop(Colors.Cyan, 1));
                }
                else if (tube.Anchor == "Target")
                {
                    brush.GradientStops.Add(new GradientStop(Colors.Yellow, 1));
                }
                else
                {
                    if (tube.Status == "Critical")
                    {
                        brush.GradientStops.Add(new GradientStop(Colors.Red, 1));
                    }
                    else if (tube.Status == "Plugged")
                    {
                        brush.GradientStops.Add(new GradientStop(Colors.Black, 1));
                    }
                    else
                    {
                        brush.GradientStops.Add(new GradientStop(Colors.Gray, 1));
                    }
                }

                newShape.Fill = brush;

                Canvas.SetLeft(newShape, tube.Column * _tubesheet.tubePitch / 2);
                Canvas.SetTop(newShape, _tubesheet.tubesheetHeight - (tube.Row * _tubesheet.tubePitch / 2));

                ToolTip tt = new System.Windows.Controls.ToolTip();
                tt.Content = "Row: " + tube.Row.ToString() + ", Column: " + tube.Column;
                newShape.ToolTip = tt;
                newShape.Tag = tube.Id;

                TubeSheetElements.Add(newShape);
            }
            //if(_walker.GetRotation != null)
            reDrawWalker();
        }

        public void resetTubeSheet()
        {

            while (TubeSheetElements.Count > 0)
            {
                TubeSheetElements.RemoveAt(0);
            }

            foreach (Tube tube in _tubesheet.tubes)
            {

                Shape newShape = new Ellipse() { Height = _tubesheet.tubeDiameter / 2, Width = _tubesheet.tubeDiameter / 2 };

                RadialGradientBrush brush = new RadialGradientBrush();

                if (tube.Anchor == "Start")
                {
                    brush.GradientStops.Add(new GradientStop(Colors.Cyan, 1));
                }
                else if (tube.Anchor == "Target")
                {
                    brush.GradientStops.Add(new GradientStop(Colors.Yellow, 1));
                }
                else
                {
                    if (tube.Status == "Critical")
                    {
                        brush.GradientStops.Add(new GradientStop(Colors.Red, 1));
                    }
                    else if (tube.Status == "Plugged")
                    {
                        brush.GradientStops.Add(new GradientStop(Colors.Black, 1));
                    }
                    else
                    {
                        brush.GradientStops.Add(new GradientStop(Colors.Gray, 1));
                    }
                }

                newShape.Fill = brush;

                Canvas.SetLeft(newShape, tube.Column * _tubesheet.tubePitch / 2);
                Canvas.SetTop(newShape, _tubesheet.tubesheetHeight - (tube.Row * _tubesheet.tubePitch / 2));

                ToolTip tt = new System.Windows.Controls.ToolTip();
                tt.Content = "Row: " + tube.Row.ToString() + ", Column: " + tube.Column;
                newShape.ToolTip = tt;
                newShape.Tag = tube.Id;

                TubeSheetElements.Add(newShape);
            }
        }

        private bool isWalkerInTubeSheetArea(){
            if (_walker.GetMainAxisHeadTubeColumn() <= 0 ||
                _walker.GetMainAxisHeadTubeRow() <= 0 ||
                _walker.mainAxisPincerBTubeColumn <= 0 ||
                _walker.mainAxisPincerBTubeRow <= 0  ||
                _walker.secondaryAxisPincerATubeColumn <= 0 ||
                _walker.secondaryAxisPincerATubeRow <= 0 ||
                _walker.secondaryAxisPincerBTubeColumn <= 0 ||
                _walker.secondaryAxisPincerBTubeRow <= 0 ||
                _walker.GetMainAxisHeadTubeColumn() <= 0 ||
                _walker.GetMainAxisHeadTubeRow() > _tubesheet.tubesheetSideTubeCount ||
                _walker.mainAxisPincerBTubeColumn > _tubesheet.tubesheetSideTubeCount ||
                _walker.mainAxisPincerBTubeRow > _tubesheet.tubesheetSideTubeCount ||
                _walker.secondaryAxisPincerATubeColumn > _tubesheet.tubesheetSideTubeCount ||
                _walker.secondaryAxisPincerATubeRow > _tubesheet.tubesheetSideTubeCount ||
                _walker.secondaryAxisPincerBTubeColumn > _tubesheet.tubesheetSideTubeCount ||
                _walker.secondaryAxisPincerBTubeRow > _tubesheet.tubesheetSideTubeCount)
            {
                return false;
            }else{
                return true;
            }

        }

        #endregion

    }
}
