﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace WalkerDemo.Models
{
    class TubeSheet
    {
        public double tubeDiameter = 16.8;
        public double tubePitch = 27.43;
        public int tubesheetSideTubeCount = 56;
        public int tubesheetHeight;
        private enum TubeStatus { Unknown, Plugged, Critical };
        private enum TubeAnchor { Start, Target, None };

        public IList<Tube> tubes = new List<Tube>();

        public Tube StartTube = null;
        public Tube TargetTube = null;

        public TubeSheet()
        {
            tubesheetHeight = (int)Math.Floor((tubesheetSideTubeCount + 1) * (tubePitch / 2));
        }


        public void InitiateTubesFromXML()
        {
            string xmlUrl = "Tubesheet.txt";

            // Read xml from file and generate tube data

            XmlDocument doc = new XmlDocument();
            doc.Load(xmlUrl);

            XmlNode tubesheetModel = doc.SelectSingleNode("TubesheetModel");
            XmlNode tubesCollection = tubesheetModel.SelectSingleNode("Tubes");
            XmlNodeList tubesNodes = tubesCollection.SelectNodes("Tube");
            int index = 1;

            tubes.Clear();
            StartTube = null;
            TargetTube = null;

            foreach (XmlNode tube in tubesNodes)
            {
                int row = Int16.Parse(tube.SelectSingleNode("Row").FirstChild.Value);
                int column = Int16.Parse(tube.SelectSingleNode("Column").FirstChild.Value);
                string status = tube.SelectSingleNode("Status").FirstChild.Value;

                Tube newTube = new Tube(index, row, column, status);
                tubes.Add(newTube);
                index++;
            }
        }


        #region HELPERS

        public void ResetTubeStartAnchors()
        {
            foreach (Tube tube in tubes)
            {
                if (tube.Anchor == "Start")
                {
                    tube.Anchor = "None";
                }
            }
        }

        public void ResetTubeTargetAnchors()
        {
            foreach (Tube tube in tubes)
            {
                if (tube.Anchor == "Target")
                {
                    tube.Anchor = "None";
                }
            }
        }

        public Tube GetTubeByID(int id)
        {
            Tube tube = (from t in tubes
                         where t.Id == id
                         select t).SingleOrDefault();

            return tube;
        }

        public Tube GetTubeByRowAndColumn(int row, int column)
        {
            foreach (Tube tube in tubes)
            {
                if (tube.Row == row && tube.Column == column)
                {
                    return tube;
                }
            }
            return null;
        }

        public double GetTubeY(int tubeRow, double areaHeight)
        {
            return areaHeight - (tubeRow * tubePitch / 2) + 4;
        }

        public double GetTubeX(int tubeColumn)
        {
            return tubeColumn * tubePitch / 2 + 4;
        }

        #endregion
    }
}
