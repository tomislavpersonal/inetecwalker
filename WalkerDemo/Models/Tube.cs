﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WalkerDemo.Models
{
    class Tube
    {
        public Tube(int id, int row, int column, string status)
        {
            Id = id;
            Row = row;
            Column = column;
            Status = status;
            Anchor = "None";
        }

        public int Id { get; set; }
        public int Row { get; set; }
        public int Column { get; set; }
        public string Status { get; set; }
        public string Anchor { get; set; }
    }
}
