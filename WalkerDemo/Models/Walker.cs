﻿using System;

namespace WalkerDemo.Models
{
    class Walker
    {
        private int mainAxisHeadTubeRow { get; set; }
        private int mainAxisHeadTubeColumn { get; set; }
        private int walkerOrientation { get; set; }
        private int mainAxisRotationAngle { get; set; }
        private int mainAxisTranslation { get; set; }
        private int secondaryAxisTranslation { get; set; }

        // INFO: possible to use WalkerDemo.Tube or System.Windows.Point classes to store Pincer coordinates,
        // but want to keep Model indepentant of system

        // TODO: set to private, and calculate from model orientation instead of setting pincers coordinates directly

        public int mainAxisPincerATubeRow { get; set; }
        public int mainAxisPincerATubeColumn { get; set; }
        public int mainAxisPincerBTubeRow { get; set; }
        public int mainAxisPincerBTubeColumn { get; set; }

        public int secondaryAxisPincerATubeRow { get; set; }
        public int secondaryAxisPincerATubeColumn { get; set; }
        public int secondaryAxisPincerBTubeRow { get; set; }
        public int secondaryAxisPincerBTubeColumn { get; set; }

        public int MainAxisPincerATubeRow
        {
            get{return mainAxisPincerATubeRow;}
            set{}
        }

        public int MainAxisPincerATubeColumn
        {
            get { return mainAxisPincerATubeColumn; }
            set { }
        }

        public int MainAxisPincerBTubeRow
        {
            get { return mainAxisPincerBTubeRow; }
            set { }
        }

        public int MainAxisPincerBTubeColumn
        {
            get { return mainAxisPincerBTubeColumn; }
            set { }
        }

        public int SecondaryAxisPincerATubeRow
        {
            get { return secondaryAxisPincerATubeRow; }
            set { }
        }

        public int SecondaryAxisPincerATubeColumn
        {
            get { return secondaryAxisPincerATubeColumn; }
            set { }
        }

        public int SecondaryAxisPincerBTubeRow
        {
            get { return secondaryAxisPincerBTubeRow; }
            set { }
        }

        public int SecondaryAxisPincerBTubeColumn
        {
            get { return secondaryAxisPincerBTubeColumn; }
            set { }
        }



        public Walker(int orientaion)
        {
            walkerOrientation = orientaion;

        }


        #region STATE READERS

        public int GetMainAxisHeadTubeRow()
        {
            return mainAxisHeadTubeRow;
        }

        public int GetMainAxisHeadTubeColumn()
        {
            return mainAxisHeadTubeColumn;
        }

        public int SetWalkerOrientation()
        {
            return walkerOrientation;
        }

        public int GetRotation()
        {
            return mainAxisRotationAngle;
        }

        public int GetMainTranslation()
        {
            return mainAxisTranslation;
        }

        public int GetSecondaryTranslation()
        {
            return secondaryAxisTranslation;
        }

        #endregion


        #region POSITIONING AND MOVEMENT

        public void SetMainAxisHeadTubeRow(int value)
        {
            mainAxisHeadTubeRow = value;
        }

        public void SetMainAxisHeadTubeColumn(int value)
        {
            mainAxisHeadTubeColumn = value;
        }

        public void SetWalkerOrientation(int value)
        {
            walkerOrientation = value;
        }

        public void Rotate(int value)
        {
            mainAxisRotationAngle = value;
        }

        public void TranslateMainAxis(int value)
        {
            mainAxisTranslation = value;
        }

        public void TranslateSecondaryAxis(int value)
        {
            secondaryAxisTranslation = value;
        }

        #endregion


        #region HELPERS

        public void CalculateWalkerAxes()
        {
            switch (walkerOrientation)
            {
                case 0: // head up left

                    mainAxisPincerATubeRow = mainAxisHeadTubeRow - 1;
                    mainAxisPincerATubeColumn = mainAxisHeadTubeColumn + 1;
                    mainAxisPincerBTubeRow = mainAxisHeadTubeRow - 15;
                    mainAxisPincerBTubeColumn = mainAxisHeadTubeColumn + 15;

                    switch (mainAxisRotationAngle)
                    {
                        case -45:  // Secondary Axis horizontal, PincerA left
                            secondaryAxisPincerATubeRow = mainAxisHeadTubeRow - 8;
                            secondaryAxisPincerATubeColumn = mainAxisHeadTubeColumn - 4;

                            secondaryAxisPincerBTubeRow = mainAxisHeadTubeRow - 8;
                            secondaryAxisPincerBTubeColumn = mainAxisHeadTubeColumn + 20;

                            break;
                        case 45:   // Secondary Axis vertical, PincerA down
                            secondaryAxisPincerATubeRow = mainAxisHeadTubeRow + 4;
                            secondaryAxisPincerATubeColumn = mainAxisHeadTubeColumn + 8;

                            secondaryAxisPincerBTubeRow = mainAxisHeadTubeRow - 20;
                            secondaryAxisPincerBTubeColumn = mainAxisHeadTubeColumn + 8;

                            break;
                    }

                    break;

                case 1: // head up right

                    mainAxisPincerATubeRow = mainAxisHeadTubeRow - 1;
                    mainAxisPincerATubeColumn = mainAxisHeadTubeColumn - 1;

                    mainAxisPincerBTubeRow = mainAxisHeadTubeRow - 15;
                    mainAxisPincerBTubeColumn = mainAxisHeadTubeColumn - 15;


                    switch (mainAxisRotationAngle)
                    {
                        case -45:  // Secondary Axis vertical, PincerA up
                            secondaryAxisPincerATubeRow = mainAxisHeadTubeRow + 4;
                            secondaryAxisPincerATubeColumn = mainAxisHeadTubeColumn - 8;

                            secondaryAxisPincerBTubeRow = mainAxisHeadTubeRow - 20;
                            secondaryAxisPincerBTubeColumn = mainAxisHeadTubeColumn - 8;

                            break;
                        case 45:   // Secondary Axis horizontal, PincerA left
                            secondaryAxisPincerATubeRow = mainAxisHeadTubeRow - 8;
                            secondaryAxisPincerATubeColumn = mainAxisHeadTubeColumn - 20;

                            secondaryAxisPincerBTubeRow = mainAxisHeadTubeRow - 8;
                            secondaryAxisPincerBTubeColumn = mainAxisHeadTubeColumn + 4;

                            break;
                    }
                    break;

                case 2: // head down left

                    mainAxisPincerATubeRow = mainAxisHeadTubeRow + 1;
                    mainAxisPincerATubeColumn = mainAxisHeadTubeColumn + 1;

                    mainAxisPincerBTubeRow = mainAxisHeadTubeRow + 15;
                    mainAxisPincerBTubeColumn = mainAxisHeadTubeColumn + 15;

                    switch (mainAxisRotationAngle)
                    {
                        case -45:  // Secondary Axis vertical, PincerA down
                            secondaryAxisPincerATubeRow = mainAxisHeadTubeRow - 4;
                            secondaryAxisPincerATubeColumn = mainAxisHeadTubeColumn + 8;

                            secondaryAxisPincerBTubeRow = mainAxisHeadTubeRow + 20;
                            secondaryAxisPincerBTubeColumn = mainAxisHeadTubeColumn + 8;

                            break;
                        case 45:   // Secondary Axis horizontal, PincerA right
                            secondaryAxisPincerATubeRow = mainAxisHeadTubeRow + 8;
                            secondaryAxisPincerATubeColumn = mainAxisHeadTubeColumn + 20;

                            secondaryAxisPincerBTubeRow = mainAxisHeadTubeRow + 8;
                            secondaryAxisPincerBTubeColumn = mainAxisHeadTubeColumn - 4;

                            break;
                    }

                    break;

                case 3: // head down right

                    mainAxisPincerATubeRow = mainAxisHeadTubeRow + 1;
                    mainAxisPincerATubeColumn = mainAxisHeadTubeColumn - 1;

                    mainAxisPincerBTubeRow = mainAxisHeadTubeRow + 15;
                    mainAxisPincerBTubeColumn = mainAxisHeadTubeColumn - 15;

                    switch (mainAxisRotationAngle)
                    {
                        case -45:  // Secondary Axis horizontal, PincerA right
                            secondaryAxisPincerATubeRow = mainAxisHeadTubeRow + 8;
                            secondaryAxisPincerATubeColumn = mainAxisHeadTubeColumn + 4;

                            secondaryAxisPincerBTubeRow = mainAxisHeadTubeRow + 8;
                            secondaryAxisPincerBTubeColumn = mainAxisHeadTubeColumn - 20;

                            break;
                        case 45:   // Secondary Axis vertical, PincerA up
                            secondaryAxisPincerATubeRow = mainAxisHeadTubeRow + 20;
                            secondaryAxisPincerATubeColumn = mainAxisHeadTubeColumn - 8;

                            secondaryAxisPincerBTubeRow = mainAxisHeadTubeRow - 4;
                            secondaryAxisPincerBTubeColumn = mainAxisHeadTubeColumn - 8;

                            break;
                    }

                    break;

            }

        }

        public void CalculateWalkerMainAxis()
        {
            switch (walkerOrientation)
            {
                case 0: // head up left

                    mainAxisPincerATubeRow = mainAxisHeadTubeRow - 1;
                    mainAxisPincerATubeColumn = mainAxisHeadTubeColumn + 1;
                    mainAxisPincerBTubeRow = mainAxisHeadTubeRow - 15;
                    mainAxisPincerBTubeColumn = mainAxisHeadTubeColumn + 15;

                    break;

                case 1: // head up right

                    mainAxisPincerATubeRow = mainAxisHeadTubeRow - 1;
                    mainAxisPincerATubeColumn = mainAxisHeadTubeColumn - 1;

                    mainAxisPincerBTubeRow = mainAxisHeadTubeRow - 15;
                    mainAxisPincerBTubeColumn = mainAxisHeadTubeColumn - 15;

                    break;

                case 2: // head down left

                    mainAxisPincerATubeRow = mainAxisHeadTubeRow + 1;
                    mainAxisPincerATubeColumn = mainAxisHeadTubeColumn + 1;

                    mainAxisPincerBTubeRow = mainAxisHeadTubeRow + 15;
                    mainAxisPincerBTubeColumn = mainAxisHeadTubeColumn + 15;

                    break;

                case 3: // head down right

                    mainAxisPincerATubeRow = mainAxisHeadTubeRow + 1;
                    mainAxisPincerATubeColumn = mainAxisHeadTubeColumn - 1;

                    mainAxisPincerBTubeRow = mainAxisHeadTubeRow + 15;
                    mainAxisPincerBTubeColumn = mainAxisHeadTubeColumn - 15;

                    break;

            }

        }

        public void CalculateWalkerHeadAfterRotation()
        {
            switch (walkerOrientation)
            {
                case 0: // head up left

                    switch (mainAxisRotationAngle)
                    {
                        case -45:
                            mainAxisHeadTubeColumn = mainAxisHeadTubeColumn - ((mainAxisHeadTubeRow - secondaryAxisPincerATubeRow) * 2);

                            break;
                        case 45:  
                            

                            break;
                    }

                    break;

                case 1: // head up right

                    switch (mainAxisRotationAngle)
                    {
                        case -45:


                            break;
                        case 45:
                            mainAxisHeadTubeColumn = mainAxisHeadTubeColumn + ((mainAxisHeadTubeRow - secondaryAxisPincerATubeRow) * 2);

                            break;
                    }
                    break;

                //case 2: // head down left

                //    mainAxisPincerATubeRow = mainAxisHeadTubeRow + 1;
                //    mainAxisPincerATubeColumn = mainAxisHeadTubeColumn + 1;

                //    mainAxisPincerBTubeRow = mainAxisHeadTubeRow + 15;
                //    mainAxisPincerBTubeColumn = mainAxisHeadTubeColumn + 15;

                //    switch (mainAxisRotationAngle)
                //    {
                //        case -45:  // Secondary Axis vertical, PincerA down
                //            secondaryAxisPincerATubeRow = mainAxisHeadTubeRow - 4;
                //            secondaryAxisPincerATubeColumn = mainAxisHeadTubeColumn + 8;

                //            secondaryAxisPincerBTubeRow = mainAxisHeadTubeRow + 20;
                //            secondaryAxisPincerBTubeColumn = mainAxisHeadTubeColumn + 8;

                //            break;
                //        case 45:   // Secondary Axis horizontal, PincerA right
                //            secondaryAxisPincerATubeRow = mainAxisHeadTubeRow + 8;
                //            secondaryAxisPincerATubeColumn = mainAxisHeadTubeColumn + 20;

                //            secondaryAxisPincerBTubeRow = mainAxisHeadTubeRow + 8;
                //            secondaryAxisPincerBTubeColumn = mainAxisHeadTubeColumn - 4;

                //            break;
                //    }

                //    break;

                //case 3: // head down right

                //    mainAxisPincerATubeRow = mainAxisHeadTubeRow + 1;
                //    mainAxisPincerATubeColumn = mainAxisHeadTubeColumn - 1;

                //    mainAxisPincerBTubeRow = mainAxisHeadTubeRow + 15;
                //    mainAxisPincerBTubeColumn = mainAxisHeadTubeColumn - 15;

                //    switch (mainAxisRotationAngle)
                //    {
                //        case -45:  // Secondary Axis horizontal, PincerA right
                //            secondaryAxisPincerATubeRow = mainAxisHeadTubeRow + 8;
                //            secondaryAxisPincerATubeColumn = mainAxisHeadTubeColumn + 4;

                //            secondaryAxisPincerBTubeRow = mainAxisHeadTubeRow + 8;
                //            secondaryAxisPincerBTubeColumn = mainAxisHeadTubeColumn - 20;

                //            break;
                //        case 45:   // Secondary Axis vertical, PincerA up
                //            secondaryAxisPincerATubeRow = mainAxisHeadTubeRow + 20;
                //            secondaryAxisPincerATubeColumn = mainAxisHeadTubeColumn - 8;

                //            secondaryAxisPincerBTubeRow = mainAxisHeadTubeRow - 4;
                //            secondaryAxisPincerBTubeColumn = mainAxisHeadTubeColumn - 8;

                //            break;
                //    }

                //    break;

            }

        }

        #endregion
    }
}
